## DHL Parcel UK

### Requirements

```"clegginabox/pdf-merger": "dev-master"```

```"guzzlehttp/guzzle": "^6.3"```

### Setup

Works along Courier and Shipping manager packages.

Shipping rates must be set up with correct service IDs.

Relies on following config params: `courier.delivery_service_url`, 
`courier.delivery_service_test_url`, 
`courier.api_token`

Notification Url: `/dhlparcel/notify` 

Run following SQL queries for missing columns:
```sql
alter table order_shipments
	add shipment_id int null;

alter table order_shipments
	add package text null;

alter table order_picking_pool
    add boxes text null;

alter table order_picking_pool
    add courier varchar(255) null;

alter table order_picking_pool
    add collection_date datetime null;


create table order_shipment_collection
(
    id int,
    collection_date datetime null,
    collection_ref text null
);

create unique index order_shipment_collection_id_uindex
    on order_shipment_collection (id);

alter table order_shipment_collection
    add constraint order_shipment_collection_pk
        primary key (id);

alter table order_shipment_collection modify id int auto_increment;
```

### Misc

Uses 3rd party php package to merge PDF labels received by API into single printable pdf document.
This makes it easier for users to print bigger batches of labels. `tmp` directory is necessary to keep temporary generated 
PDF files before merging. Directory clears everytime labels are rendered.

### Todo

Bulk order processing might be cumbersome and can time out. Possible solutions to use Guzzle concurrency, job queue or split into ajax requests per order.


### Troubleshooting

- Labels are not generating. Or, label is partially save in DB. `order_shipments.label` change column type to `MEDIUM TEXT`. 

### Example projects

https://www.hoganscider.co.uk/