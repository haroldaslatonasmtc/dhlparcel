<?php

return [
    'courier_id' => 9, // DHL parcel courier ID on MTC shippming manager DB `delivery_couriers` @todo make this better
    'full_customs_zone_ids' => [
        14, // Northern Ireland
    ], // Zone IDs in shipping_zones that requires full customs declaration
];