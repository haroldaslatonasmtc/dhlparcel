<?php

namespace Mtc\Plugins\DHLParcel\Classes\Http\Controllers\Admin;

use Carbon\Carbon;
use Clegginabox\PDFMerger\PDFMerger;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Mtc\Core\Http\Controllers\Controller;
use Mtc\Orders\Order;
use Mtc\Orders\OrderShipment;
use Mtc\Plugins\DHLParcel\Classes\Collection;
use Mtc\Plugins\DHLParcel\Classes\OrderPickingPool;
use Mtc\ShippingManager\TableRate;

class ParcelController extends Controller
{

    /**
     * Determine mtc delivery API endpoint URL
     *
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed|void
     */
    protected function getEndpoint()
    {
        if (getenv('APP_ENV') === 'staging') {
            return config('courier.delivery_service_test_url');
        }
        return config('courier.delivery_service_url');
    }

    public function index(Request $request)
    {
        \Mtc\Core\Gate\Auth::loadUser();
        $this->page_meta['title'] = 'DHL Parcel';

        if ($request->isMethod('post')) {
            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->post($this->getEndpoint() . 'collections',
                    [
                        RequestOptions::FORM_PARAMS => [
                            'api_token' => config('courier.api_token'),
                            'collection_date' => $request->input('collection_date'),
                            'courier_id' => config('dhlparcel.courier_id')
                        ]
                    ]);
            } catch (GuzzleException $ge) {
                $json_response = json_decode($ge->getResponse()->getBody()->getContents());
                return redirect(route('plugin.dhlparcel.admin.index'))
                    ->with('error', $json_response->message);
            }
            $json_response = json_decode($response->getBody()->getContents());
            if (json_last_error() <= 0) {
                $collection = Collection::where('collection_ref', $json_response->data->collectionJobNumber)->first();
                if (!$collection) {
                    Collection::create([
                        'collection_ref' => $json_response->data->collectionJobNumber,
                        'collection_date' => $request->input('collection_date')
                    ]);
                }

                return redirect(route('plugin.dhlparcel.admin.index'))
                    ->with('success',
                        'Collection requested: ' . $json_response->data->bookingMessage .
                        ' (Request ID: ' . $json_response->data->collectionJobNumber . ')');
            } else {
                return redirect(route('plugin.dhlparcel.admin.index'))
                    ->with('error', 'Error occurred try again');
            }
        }

        return template('dhlparcel/admin/index.twig', [
            'page_meta' => $this->page_meta,
            'today' => Carbon::today()->format('Y-m-d'),
            'collections' => Collection::orderByDesc('collection_date')->limit(31)->get()
        ]);
    }

    /**
     * Process batch of orders. Send to MTC delivery API for further processing.
     *
     *
     * @param Request $request
     * @param $collection_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function process(Request $request, $collection_id)
    {
        \Mtc\Core\Gate\Auth::loadUser();

        $collection = Collection::find($collection_id);

        if (!$collection) {
            return redirect()->route('plugin.dhlparcel.admin.index')
                ->with('error', 'Collection ID does not exist');
        }

        if ($request->isMethod('post')) {

            $errors = [];

            $orders_sent = 0;
            foreach ($request->input('orders') as $order_id) {

                /** @var Order $order */
                $order = Order::find($order_id);
                $shipping = TableRate::find($order->deliverySurcharge->surcharge_id);

                $errors['Order-' . $order->id] = [];
                if ( empty($order->contact_number) ) {
                    $errors['Order-' . $order->id][] = "Missing Phone number";
                }

                if ( empty($order->email) ) {
                    $errors['Order-' . $order->id][] = "Missing Email";
                }

                if ( strlen($order->email) > 60 ) {
                    $errors['Order-' . $order->id][] = "Email is too long max 60 characters";
                }

                if ( strlen($order->contact_number) > 50 ) {
                    $errors['Order-' . $order->id][] = "Number is too long max 50 characters";
                }

                if ( strlen($order->billingAddress->address1) > 35 ) {
                    $errors['Order-' . $order->id][] = "Billing Address Line is too long max 35 characters";
                }

                if ( strlen($order->billingAddress->address2) > 35 ) {
                    $errors['Order-' . $order->id][] = "Billing Address Line 2 is too long max 35 characters";
                }

                if ( strlen($order->billingAddress->city) > 35 ) {
                    $errors['Order-' . $order->id][] = "Billing City Name is too long max 35 characters";
                }

                if ( strlen($order->shippingAddress->address1) > 35 ) {
                    $errors['Order-' . $order->id][] = "Shipping Address Line is too long max 35 characters";
                }

                if ( strlen($order->shippingAddress->address2) > 35 ) {
                    $errors['Order-' . $order->id][] = "Shipping Address Line 2 is too long max 35 characters";
                }

                if ( strlen($order->shippingAddress->city) > 35 ) {
                    $errors['Order-' . $order->id][] = "Shipping City Name is too long max 35 characters";
                }

                if ( count($errors['Order-' . $order->id]) > 0 ) {
                    continue;
                }
                unset($errors['Order-' . $order->id]);

                $total_weight = 0;
                $items_map = [];
                foreach ($order->items as $order_item) {
                    $item_weight = $order_item->data['weight'];
                    $items_map[] = [
                        'description' => 'Beverage product - ' . $order_item->name,
                        'name' => $order_item->name,
                        'price' => $order_item->paid_price->original(),
                        'quantity' => $order_item->quantity,
                        'weight' => $item_weight <= 0? 1: $item_weight,
                    ];
                    $total_weight += $order_item->data['weight'];
                }
                if ( $total_weight == 0 ) {
                    $total_weight = 1;
                }

                $order_data = [
                    'api_token' => config('courier.api_token'),
                    'service_id' => $shipping->delivery_service_id,
                    'id' => $order->id,
                    'info' => [
                        'email' => $order->email,
                        'contact_no' => $order->contact_number,
                    ],
                    'items' => $items_map,
                    'packages' => [
                        0 => [
                            'name' => 'Package',
                            'weight' => $total_weight,
                        ]
                    ],
                    'address' => [
                        'billing' => $order->billingAddress->toArray(),
                        'shipping' => $order->shippingAddress->toArray(),
                    ],
                    'courier' => [
                        'collection_date' => $collection->collection_date->format('Y-m-d'),
                    ],
                    'additional_options' => [
                        'shipping_cost' => $order->surcharges()->sum('surcharge_amount'),
                        'total_order_value' => $order->cost_total->original(),
                        'order_paid_at' => $order->paid_at,
                        'collection_request_id' => $collection->collection_ref,
                        'custom_declaration_type' => in_array($shipping->zone_id, config('dhlparcel.full_customs_zone_ids'))
                    ],
                ];

                $client = new \GuzzleHttp\Client();
                try {
                    $client->post($this->getEndpoint() . 'orders',
                        [
                            RequestOptions::JSON => $order_data
                        ]);
                    $order->shipment_sent_api = 1;
                    $order->save();

                    OrderPickingPool::create([
                        'order_id' => $order_id,
                        'collection_date' => $collection->collection_date,
                        'courier' => 'dhlparceluk'
                    ]);
                    $orders_sent++;
                } catch (GuzzleException $ge) {
                    return redirect()
                        ->route('plugin.dhlparcel.admin.collections', ['collection_id' => $collection->id])
                        ->with('error', $ge->getMessage());
                }
            }

            $error_string = '';
            if ( count($errors) > 0 ) {
                $error_string = "\n Some errors found and orders skipped:\n";
                foreach ($errors as $order_key => $error) {
                    $error_string .= "{$order_key} - " . join(', ', $error);
                }
            }

            $progress = $orders_sent . '/' . count($request->input('orders'));


            return redirect()
                ->route('plugin.dhlparcel.admin.collections', ['collection_id' => $collection->id])
                ->with('success', $progress . ' Orders have been sent to DHL Parcel UK' . $error_string);
        }

        $this->page_meta['title'] = 'DHL Parcel - Process Shipments for collection ' . $collection->collection_date->format('Y-m-d') . " ({$collection->collection_ref})";

        return template('dhlparcel/admin/process.twig', [
            'page_meta' => $this->page_meta,
            'orders' => Order::where('paid_at', '>=', Carbon::today()->subDays(30))
                ->where('status_id', 1)
                ->where('shipment_sent_api', 0)
                ->whereHas('surcharges', function($query) {
                    return $query->where('surcharge_type', 'delivery');
                })
                ->get(),
            'collection' => $collection
        ]);
    }

    /**
     * Display all collection request and request/create new collection requests
     *
     * @param Request $request
     * @param $collection_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function collections(Request $request, $collection_id)
    {
        \Mtc\Core\Gate\Auth::loadUser();

        $collection = Collection::find($collection_id);

        if (!$collection) {
            return redirect()->route('plugin.dhlparcel.admin.index')
                ->with('error', 'Collection ID does not exist');
        }

        $this->page_meta['title'] = 'DHL Parcel - View Collection orders';
        return template('dhlparcel/admin/collection.twig', [
            'page_meta' => $this->page_meta,
            'orders' => $collection->orders(),
            'collection' => $collection
        ]);
    }

    /**
     * Consume notification webhook
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function update(Request $request)
    {
        if ($request->server('HTTP_AUTH') !== config('courier.api_token')) {
            return abort(400);
        }
        if ( $request->input('type') != 'error' ) {
            collect($request->input())
                ->each(function ($shipment) {
                    OrderShipment::query()
                        ->updateOrCreate([
                            'order_id' => $shipment['order_id'],
                        ], [
                            'order_id' => $shipment['order_id'],
                            'courier_name' => $shipment['courier'] . ' - ' . $shipment['service'],
                            'exported_at' => Carbon::createFromFormat(Carbon::DEFAULT_TO_STRING_FORMAT, $shipment['created_at']),
                            'label' => $shipment['label'],
                            'label_format' => $shipment['label_format'],
                            'status' => $shipment['status'],
                            'service_id' => $shipment['service_id'],
                            'tracking_number' => $shipment['tracking_no'],
                            'shipment_confirmed_at' => Carbon::now(),
                            'shipment_data' => $shipment['payload'],
                        ]);
                    /** @var Order $o */
                    $o = Order::find($shipment['order_id']);
                    if ( $o ) {
                        $user = json_encode([
                            "name" => "DHL Parcel plugin"
                        ]);
                        $o->tracking_data = [
                            'courier' => 'dhl',
                            'code' => $shipment['service_id']
                        ];
                        $o->history()
                            ->create([
                                'triggered_by' => 'DHL Parcel plugin',
                                'name' => 'Set Tracking details',
                                'level' => 'success',
                                'details' => collect($o->tracking_data)
                                    ->map(function ($value, $key) {
                                        return "{$key}: {$value}";
                                    })->implode("\n"),
                            ]);

                        $o->changeStatus(3, json_decode($user));
                    }
                });
        }
        return response('Accepted');
    }

    /**
     * Render label for a single order within a collection
     *
     * @param Request $request
     * @param $order_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function label(Request $request, $order_id)
    {
        $merge = new PDFMerger;
        $shipments = OrderShipment::where('order_id', $order_id)
            ->get();
        if ($shipments->count() > 0) {
            // Remove old TMP files
            array_map('unlink', glob(dirname(__DIR__, 4) . '/tmp/tmp_*.pdf'));
            foreach ($shipments as $shipment) {
                $tmp_file_name = dirname(__DIR__, 4) . '/tmp/tmp_' . $shipment->id . '.pdf';
                file_put_contents($tmp_file_name, base64_decode($shipment->label));
                $merge->addPDF($tmp_file_name);
            }
            return response($merge->merge())
                ->header('Content-Type', 'application/pdf');

        } else {
            return redirect()
                ->back()
                ->with('error', 'No labels found');
        }
    }

    /**
     * Render all labels for a collection
     *
     * @param Request $request
     * @param $collection_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function collections_labels(Request $request, $collection_id)
    {
        $collection = Collection::find($collection_id);
        if ($collection) {
            $merge = new PDFMerger;
            // Remove old TMP files
            array_map('unlink', glob(dirname(__DIR__, 4) . '/tmp/tmp_*.pdf'));
            $pdfs = 0;
            foreach ($collection->orders() as $collcection_order) {
                $shipments = OrderShipment::where('order_id', $collcection_order->order_id)
                    ->get();
                foreach ($shipments as $shipment) {
                    $tmp_file_name = dirname(__DIR__, 4) . '/tmp/tmp_' . $shipment->id . '.pdf';
                    file_put_contents($tmp_file_name, base64_decode($shipment->label));
                    $merge->addPDF($tmp_file_name);
                    $pdfs++;
                }
            }
            if ($pdfs > 0) {
                return response($merge->merge(), 200, [
                    'Content-Type' => 'application/pdf'
                ]);
            } else {
                return redirect()
                    ->route('plugin.dhlparcel.admin.index')
                    ->with('error', 'No labels found');
            }
        } else {
            return redirect()
                ->route('plugin.dhlparcel.admin.index')
                ->with('error', 'Collection request not found');
        }
    }

    public function reset(Request $request, $collection_id, $order_id)
    {
        $order_picking = OrderPickingPool::where('order_id', $order_id)->first();
        if (Collection::find($collection_id)->count() > 0 && $order_picking) {

            $order = $order_picking->order;
            $order->shipment_sent_api = 0;
            $order->save();

            $order_picking->delete();
        }
        return redirect()
            ->back()
            ->with('success', 'Order Has been reset, you may try processing it again.');
    }
}