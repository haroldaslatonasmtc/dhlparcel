<?php

namespace Mtc\Plugins\DHLParcel\Classes\Providers;

use Carbon\Laravel\ServiceProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use Mtc\Plugins\NewsletterSignup\Classes\CampaignMonitor;
use Mtc\Plugins\NewsletterSignup\Classes\EzineSignUp;
use Mtc\Plugins\NewsletterSignup\Classes\Seeds\DefaultNewsletterListSeed;
use Mtc\Plugins\NewsletterSignup\Classes\SignUpInterface;
use Mtc\Plugins\NewsletterSignup\Classes\MailChimp;
use DrewM\MailChimp\MailChimp as DrewMailChimp;

/**
 * NewsletterSignup Service provider
 *
 * Initializes the services for Newsletter Sign-up
 *
 * @author Martins Fridenbergs <martins.fridenbergs@mtcmedia.co.uk>
 * @version 0.4
 */
class DHLParcelServiceProvider extends ServiceProvider
{
    /**
     * Bind the integration which will be used to register newsletter sign-ups with the app instance
     * This is to ensure correct driver is used for actions
     */
    public function register()
    {
        $this->mergeConfigFrom(dirname(__DIR__, 2) . '/config/dhlparcel.php', 'dhlparcel');
    }

    /**
     * Boot the service
     */
    public function boot()
    {
        Route::middleware('web')->group(dirname(__DIR__, 2) . '/routes.php');
    }
}
