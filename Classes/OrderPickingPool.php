<?php

namespace Mtc\Plugins\DHLParcel\Classes;

use Illuminate\Database\Eloquent\Model;
use Mtc\Orders\Order;
use Mtc\Orders\OrderShipment;

class OrderPickingPool extends Model
{
    /**
     * @var string Model table name
     */
    protected $table = 'order_picking_pool';

    /**
     * @var array The attributes that are mass assignable.
     */
    protected $fillable = [
        'order_id',
        'date_handled',
        'batch',
        'courier',
        'synced',
        'printed',
        'boxes',
        'collection_date'
    ];

    protected $casts = [
        'boxes' => 'array',
        'collection_date' => 'date'
    ];

    public function order() {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }
    public function shipments() {
        return$this->hasMany(OrderShipment::class, 'order_id', 'order_id');
    }
}