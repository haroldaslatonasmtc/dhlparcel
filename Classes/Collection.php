<?php

namespace Mtc\Plugins\DHLParcel\Classes;

class Collection extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = 'order_shipment_collection';
    protected $fillable = [
        'collection_ref',
        'collection_date',
    ];

    public $casts = [
        'collection_date' => 'date'
    ];

    public function orders()
    {
        return OrderPickingPool::where('collection_date', $this->collection_date)->get();
    }
}