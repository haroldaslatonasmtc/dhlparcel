<?php
namespace Mtc\Plugins\DHLParcel;
use App\CourierWebHookController;
use Illuminate\Routing\Router;
use Mtc\Plugins\DHLParcel\Classes\Http\Controllers\Admin\ParcelController;

/** @var Router $router */

$router->group([
    'namespace' => 'Mtc\Plugins\DHLParcel\Classes\Http\Controllers',
], function (Router $router) {
    $router->group([
        'prefix' => '/admin/plugins/dhlparcel',
        'as' => 'plugin.dhlparcel.admin.'
    ], function (Router $router) {
        $router->match(['get', 'post'], '/', [ParcelController::class, 'index'])
            ->name('index');
        $router->match(['get', 'post'],'/process/{collection_id}', [ParcelController::class, 'process'])
            ->name('process');
        $router->match(['get', 'post'], '/collections/{collection_id}', [ParcelController::class, 'collections'])
            ->name('collections');
        $router->match(['get'], '/label/{order_id}', [ParcelController::class, 'label'])
            ->name('label');
        $router->match(['get'], '/collections/{collection_id}/labels', [ParcelController::class, 'collections_labels'])
            ->name('collections_labels');
        $router->match(['get'], '/collections/{collection_id}/reset/{order_id}', [ParcelController::class, 'reset'])
            ->name('reset-order');
    });
});

$router->any('/dhlparcel/notify', [ParcelController::class, 'update']);